package com.valiit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Visits {

    public static void main(String[] args) throws IOException {
        List<String> visitLines = readFileLines(args[0]);
        List<DailyVisitInfo> dailyVisits = visitLines.stream().map(DailyVisitInfo::new).collect(Collectors.toList());

        System.out.println("Kõige rohkem oli külastajaid: " + dailyVisits.stream().max(Comparator.comparingInt(DailyVisitInfo::getVisitCount)).get().getDate());
        System.out.println();
        System.out.println("Külastuspäevad populaarsuse järgi:");
        Collections.sort(dailyVisits, Comparator.comparingInt(DailyVisitInfo::getVisitCount));
        dailyVisits.stream().forEach(dailyVisit -> System.out.printf("%s (%d)\n", dailyVisit.getDate(), dailyVisit.getVisitCount()));
    }

    private static List<String> readFileLines(String file) throws IOException {
        Path path = Paths.get(file);
        return Files.readAllLines(path);
    }

    static class DailyVisitInfo {

        private String date;
        private int visitCount;

        public DailyVisitInfo(String dailyVisits) {
            String[] csvParts = dailyVisits.split(",");
            this.date = csvParts[0].trim();
            this.visitCount = Integer.parseInt(csvParts[1].trim());
        }

        public String getDate() {
            return date;
        }

        public int getVisitCount() {
            return visitCount;
        }
    }
}
